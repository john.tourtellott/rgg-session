//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_session_rgg_operators_ExportInp_h
#define pybind_smtk_session_rgg_operators_ExportInp_h

#include <pybind11/pybind11.h>

#include "smtk/session/rgg/operators/ExportInp.h"

#include "smtk/operation/XMLOperation.h"

namespace py = pybind11;

PySharedPtrClass< smtk::session::rgg::ExportInp, smtk::operation::XMLOperation > pybind11_init_smtk_session_rgg_ExportInp(py::module &m)
{
  PySharedPtrClass< smtk::session::rgg::ExportInp, smtk::operation::XMLOperation > instance(m, "ExportInp");
  instance
    .def(py::init<::smtk::session::rgg::ExportInp const &>())
    .def(py::init<>())
    .def("deepcopy", (smtk::session::rgg::ExportInp & (smtk::session::rgg::ExportInp::*)(::smtk::session::rgg::ExportInp const &)) &smtk::session::rgg::ExportInp::operator=)
    .def_static("create", (std::shared_ptr<smtk::session::rgg::ExportInp> (*)()) &smtk::session::rgg::ExportInp::create)
    .def_static("create", (std::shared_ptr<smtk::session::rgg::ExportInp> (*)(::std::shared_ptr<smtk::session::rgg::ExportInp> &)) &smtk::session::rgg::ExportInp::create, py::arg("ref"))
    .def("shared_from_this", (std::shared_ptr<smtk::session::rgg::ExportInp> (smtk::session::rgg::ExportInp::*)()) &smtk::session::rgg::ExportInp::shared_from_this)
    .def("shared_from_this", (std::shared_ptr<const smtk::session::rgg::ExportInp> (smtk::session::rgg::ExportInp::*)() const) &smtk::session::rgg::ExportInp::shared_from_this)
    ;
  return instance;
}

#endif
