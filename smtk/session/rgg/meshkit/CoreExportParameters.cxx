//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/meshkit/CoreExportParameters.h"

namespace smtk
{
namespace session
{
namespace rgg
{

CoreExportParameters::CoreExportParameters()
{
  this->init();
}

void CoreExportParameters::init()
{
  // FIXME: initialize it based on core type
  //this->GeometryType = "Hexagonal";
  this->Geometry = "Volume";
  this->MergeTolerance = 1e-3;
}

} // namespace rgg
} //namespace session
} // namespace smtk
