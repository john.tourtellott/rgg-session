//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/session/rgg/meshkit/AssyExportParameters.h"

namespace smtk
{
namespace session
{
namespace rgg
{

AssyExportParameters::AssyExportParameters()
{
  this->init();
}

void AssyExportParameters::init()
{
  this->MeshType = "Hex";
  this->StartPinId = -1;
  this->HBlock = std::make_tuple(-1, -1.0, -1.0);
  // FIXME: initialize it based on core type
  //this->GeometryType = "Hexagonal";
  this->Geometry = "Volume";
  this->MergeTolerance = 1e-3;
  this->RadialMeshSize = 0.3;
  // By default not used
  this->TetMeshSize = -1.0;
  this->AxialMeshSize = -1.0;
  this->EdgeInterval = -1.0;
  this->Center = true;

  this->NeumannSet_StartId = -1;
  this->MaterialSet_StartId = -1;
  this->NumSuperBlocks = -1;
  this->List_MaterialSet_StartId = -1;
  this->List_NeumannSet_StartId = -1;
}

} // namespace rgg
} //namespace session
} // namespace smtk
