//=============================================================================
// Copyright (c) Kitware, Inc.
// All rights reserved.
// See LICENSE.txt for details.
//
// This software is distributed WITHOUT ANY WARRANTY; without even
// the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#include "smtk/session/rgg/operators/OperationHelper.h"

#include "smtk/session/rgg/operators/ReadRXFFile.h"

#include "smtk/io/Logger.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/session/rgg/Assembly.h"
#include "smtk/session/rgg/Core.h"
#include "smtk/session/rgg/Duct.h"
#include "smtk/session/rgg/Material.h"
#include "smtk/session/rgg/Pin.h"

#include "smtk/session/rgg/operators/CreateModel.h"
#include "smtk/session/rgg/operators/EditAssembly.h"
#include "smtk/session/rgg/operators/EditPin.h"
#include "smtk/session/rgg/operators/EditDuct.h"

#include "smtk/session/rgg/json/jsonAssembly.h"
#include "smtk/session/rgg/json/jsonCore.h"
#include "smtk/session/rgg/json/jsonDuct.h"
#include "smtk/session/rgg/json/jsonPin.h"

#include "smtk/model/EntityRef.h"
#include "smtk/model/Group.h"
#include "smtk/model/Instance.h"
#include "smtk/model/Model.h"
#include "smtk/model/Resource.h"

#include <limits>

using rggAssembly = smtk::session::rgg::Assembly;
using rggCore = smtk::session::rgg::Core;
using rggDuct = smtk::session::rgg::Duct;
using rggPin = smtk::session::rgg::Pin;
using json = nlohmann::json;

static const double cos30 = 0.86602540378443864676372317075294;
static const double cos60 = 0.5;
// static const int degreesHex[6] = { -120, -60, 0, 60, 120, 180 };
// static const int degreesRec[4] = { -90, 0, 90, 180 };
// 0', 60', 120, 180', 240', 300'
static const double cosSinAngles[6][2] = { { 1.0, 0.0 }, { cos60, -cos30 }, { -cos60, -cos30 },
  { -1.0, 0.0 }, { -cos60, cos30 }, { cos60, cos30 } };

namespace
{
// Calculate the x,y coordinates of the current pin in the hex grid
std::tuple<double, double, double> calculateHexPinCoordinate(
  const double& spacing, const std::pair<int, int>& ringAndLayer)
{
  // The index order of layer is clockwise, starting from upper left corner of the hex.
  // It's RGG's order and not ideal...
  double x{0}, y{0}, z{0};
  int ring = ringAndLayer.first;
  int layer = ringAndLayer.second;
  if (ring == 0)
  {
    x = y = 0;
  }
  else
  {
    int remainder = layer % ring;
    int modulus = layer / ring;

    double eL = ring * spacing;                       // edge length
    double xBT = -eL * cos60 + eL / (ring)*remainder; // x before transform
    double yBT = eL * cos30;                          // y befor transform
    // Apply rotation if needed. In order to avoid sin/cos calculation, we use
    // predefined values;
    double cosValue = cosSinAngles[modulus][0];
    double sinValue = cosSinAngles[modulus][1];

    x = xBT * cosValue - yBT * sinValue;
    y = yBT * cosValue + xBT * sinValue;
  }
  return std::make_tuple(x, y, z);
}

// FIXME: This function is duplicated in smtkRGGEditCoreView
// Calculate the x,y coordinates of the current assy in the hex grid
std::pair<double, double> calculateHexAssyCoordinate(const double& spacing, const int& ring, const int& layer)
{
  double x, y;
  // The index order of layer is clockwise, starting from upper left corner of the hex.
  // It's RGG's order and not ideal...
  if (ring == 0)
  {
    x = y = 0;
  }
  else
  {
    int remainder = layer % ring;
    int modulus = layer / ring;

    double eL = ring * spacing;                       // edge length
    double xBT = -eL * cos60 + eL / (ring)*remainder; // x before transform
    double yBT = eL * cos30;                          // y befor transform
    // Apply rotation if needed. In order to avoid sin/cos calculation, we use
    // predefined values;
    double cosValue = cosSinAngles[modulus][0];
    double sinValue = cosSinAngles[modulus][1];

    double x0 = xBT * cosValue - yBT * sinValue;
    double y0 = yBT * cosValue + xBT * sinValue;

    // Rotate 330 degree due to the fact that the orientations do not match in
    // the render view and schema planner
    // sin330 = -cos60 and cos330 = cos30;
    x = x0 * cos30 - y0 * (-cos60);
    y = y0 * cos30 + x0 * (-cos60);
  }
  return std::make_pair(x, y);
}
}

namespace smtk
{
namespace session
{
namespace rgg
{

size_t materialNameToIndex(std::string materialN, const std::vector<std::string>& materialList)
{
  auto iter = std::find(materialList.begin(), materialList.end(), materialN);
  if (iter != materialList.end())
  {
    size_t pos = std::distance(materialList.begin(), iter);
    return pos;
  }
  if (!materialN.empty())
  {
    smtkErrorMacro(smtk::io::Logger::instance().instance(), "Cannot find index for"
                                                            " material '"
        << materialN << "'. Set it to be the first defined material");
  }
  return 0;
}

bool OperationHelper::calculateEntityCoordsInCore(Core& core, smtk::model::ResourcePtr resource)
{
  // Pins and ducts
  // Calculate the starting point first
  bool isHex = (core.geomType() == Core::GeomType::Hex);
  std::pair<int, int> latticeSize = core.latticeSize();
  std::pair<double, double> spacing = core.ductThickness();
  auto& assyToLayout = core.layout();
  double baseX, baseY;
  if (!isHex)
  { // Use the cartesian coordinate where the starting point is located
    // at left bottom
    baseX = -1 * spacing.first * (static_cast<double>(latticeSize.first) / 2 - 0.5);
    baseY = -1 * spacing.second * (static_cast<double>(latticeSize.second) / 2 - 0.5);
  }
  else
  { // Spacing is the allowable max distance between two adjacent assembly centers
    // Use the cartesian coordinate where the starting point is located at
    // the origin point.
    baseX = baseY = 0.0; // Ignored by calculateHexAssyCoordinate for now
  }

  // Map pins&ducts to their placements
  Core::UuidToCoordinates pdToCoords, assyToCoords;
  // Loop through each assembly, calculate the new global coords for its duct and pins
  for (auto iter = assyToLayout.begin(); iter != assyToLayout.end(); iter++)
  { // For each assembly, retrieve its pins&duct info, apply the right transformation
    // then add it into pinDuctToLayout map

    smtk::model::EntityRef assyEntity = smtk::model::EntityRef(resource, iter->first);
    if (!assyEntity.hasStringProperty(Assembly::propDescription))
    {
      smtkErrorMacro(smtk::io::Logger::instance(), "An invalid assembly is provided"
                                                   " in the core layout");
    }
    const std::vector<std::pair<int, int>>& assyLayout = iter->second;
    Assembly assy = json::parse(assyEntity.stringProperty(Assembly::propDescription)[0]);
    smtk::common::UUID ductUUID = assy.associatedDuct();

    auto pinsToLocalCoords = assy.entityToCoordinates();

    std::vector<std::tuple<double, double, double>> assyCoordinates;
    for (size_t index = 0; index < assyLayout.size(); index++)
    {
      double x, y;
      if (isHex)
      {
        auto xAndy = calculateHexAssyCoordinate(spacing.first, assyLayout[index].first,
                                                assyLayout[index].second);
        x = xAndy.first;
        y = xAndy.second;
      }
      else
      {
        // In schema planner, x and y axis are following Qt's pattern.
        // Here we just follow the traditional coordinate convension
        x = baseX + spacing.first * assyLayout[index].first;
        y = baseY + spacing.second * assyLayout[index].second;
      }
      assyCoordinates.emplace_back(std::make_tuple(x, y, 0));
      // For each (x,y) pair, add it to every pin and duct in the current assy
      auto transformCoordsAndAddToMap = [&pdToCoords, &x, &y](
        const smtk::common::UUID& ent, std::vector<std::tuple<double, double, double>>& coords) {
        // Apply transformation
        for (size_t i = 0; i < coords.size(); i++)
        {
          // X
          std::get<0>(coords[i]) += x;
          // Y
          std::get<1>(coords[i]) += y;
        }

        if (pdToCoords.find(ent) != pdToCoords.end())
        { // TODO: Possible performance bottleneck
          pdToCoords[ent].insert(pdToCoords[ent].end(), coords.begin(), coords.end());
        }
        else
        {
          pdToCoords[ent] = coords;
        }
      };
      // Duct
      std::vector<std::tuple<double, double, double>> ductCoords = {{ 0, 0, 0 }};
      transformCoordsAndAddToMap(ductUUID, ductCoords);
      // Pins
      for (const auto& iter : pinsToLocalCoords)
      { // Make an explict copy
        std::vector<std::tuple<double, double, double>> coords = iter.second;
        transformCoordsAndAddToMap(iter.first, coords);
      }
    }
    assyToCoords.emplace(iter->first, assyCoordinates);
  }
  core.setEntityToCoordinates(assyToCoords);
  core.setPinsAndDuctsToCoordinates(pdToCoords);
  return true;
}

bool OperationHelper::calculateEntityCoordsInAssembly(Assembly& assembly,
                                                      bool isHex,
                                                      smtk::model::ResourcePtr resource)
{
  // Calculate UuidToCoordinates
  // Copy the logic from smtkRGGEditAssemblyView::apply function
  const std::pair<int,int>& latticeSize= assembly.latticeSize();
  const std::pair<double,double>& pitches = assembly.pitch();
  const auto& pinUuidToSchema = assembly.layout();
  smtk::model::EntityRef ductAux(resource, assembly.associatedDuct());
  auto thicknesses = assembly.pitch();
  std::vector<double> spacing = { 0, 0 };
  double baseX{0}, baseY{0};
  if (!isHex)
  { // Use the cartesian coordinate where the starting point is located
    // at left bottom
    // TODO: Use the pitch defined in the file?
    spacing[0] = thicknesses.first / static_cast<double>(latticeSize.first);
    spacing[1] = thicknesses.second / static_cast<double>(latticeSize.second);
    baseX = -1 * thicknesses.first / 2 + spacing[0] / 2;
    baseY = -1 * thicknesses.second / 2 + spacing[0] / 2;
  }
  else
  { // Spacing is the allowable max distance between two adjacent pin centers
    // Use the cartesian coordinate where the starting point is located at
    // the origin point.
    spacing[0] = pitches.first;
    spacing[1] = pitches.second;
    baseX = baseY = 0.0; // Ignored by calculateHexPinCoordinate for now
  }

  Assembly::UuidToCoordinates entityToCoordinates;
  for (auto iter = pinUuidToSchema.begin(); iter != pinUuidToSchema.end(); iter++)
  {
    const auto& layout = iter->second;
    std::vector<std::tuple<double, double, double>> coordinates;
    coordinates.reserve(layout.size());
    for (size_t index = 0; index < layout.size(); index++)
    {
      double x, y;
      if (isHex)
      {
        coordinates.emplace_back(calculateHexPinCoordinate(spacing[0], layout[index]));
      }
      else
      { // Question
        // In schema planner, x and y axis are exchanged. Here we just follow the traditional coordinate convension
        x = baseX + spacing[0] * layout[index].first;
        y = baseY + spacing[1] * layout[index].second;
        coordinates.emplace_back(std::make_tuple(x,y,0));
      }
    }
    // Set it in the edit assembly
    //cIAtt->findModelEntity("snap to entity")->setIsEnabled(false);
    entityToCoordinates.emplace(iter->first, std::move(coordinates));
  }
  assembly.setEntityToCoordinates(entityToCoordinates);
  return true;
}

void OperationHelper::createInstanceFromEntityAndCoords(smtk::model::ResourcePtr& resource,
                                           smtk::attribute::ComponentItemPtr& createdItem,
                                           smtk::attribute::ComponentItemPtr& modifiedItem,
                                           smtk::attribute::ComponentItemPtr& tessChangedItem,
                                           smtk::model::Group& group,
                                           const smtk::model::EntityRef& compositeAux,
                                           const std::vector<std::tuple<double, double, double>>& coordinates)
{
  auto childAuxs = compositeAux.as<smtk::model::AuxiliaryGeometry>().auxiliaryGeometries();
  for (const auto& childAux : childAuxs)
  {
    Instance instance = resource->addInstance(childAux);
    instance.setRule("tabular");
    instance.setColor(childAux.color());
    instance.setName(childAux.name()+"-instance");

    smtk::model::FloatList placements;
    placements.reserve(coordinates.size()*3);
    for (const auto& coord : coordinates)
    {
      placements.emplace_back(std::get<0>(coord));
      placements.emplace_back(std::get<1>(coord));
      placements.emplace_back(std::get<2>(coord));
    }
    instance.setFloatProperty("placements", placements);

    group.addEntity(instance);

    createdItem->appendValue(instance.component());
    modifiedItem->appendValue(childAux.component());
    tessChangedItem->appendValue(instance.component());

    // Now that the instance is fully specified, generate
    // the placement points.
    instance.generateTessellation();

  }
}

void OperationHelper::populateChildrenInPinAux(smtk::model::AuxiliaryGeometry& pinAux,
                              std::vector<smtk::model::EntityRef>& subPinAuxs,
                                          const Pin& pin)
{
  auto assignColor = [](size_t index, smtk::model::AuxiliaryGeometry& aux) {
    smtk::model::FloatList rgba = smtk::session::rgg::CreateModel::getMaterialColor(index, aux.owningModel());
    aux.setColor(rgba);
  };
  size_t numParts = pin.pieces().size();
  size_t numLayers = pin.layerMaterials().size();

  // Create auxgeom placeholders for layers and parts
  for (std::size_t i = 0; i < numParts; i++)
  {
    for (std::size_t j = 0; j < numLayers; j++)
    {
      // Create an auxo_geom for current each unit part&layer
      AuxiliaryGeometry subLayer = pinAux.resource()->addAuxiliaryGeometry(pinAux, 3);
      std::string subLName = pin.name() + smtk::session::rgg::Pin::subpartDescription +
                             std::to_string(i) +
                             Pin::layerDescription + std::to_string(j);
      subLayer.setName(subLName);
      subLayer.setStringProperty("rggType", Pin::typeDescription);
      assignColor(static_cast<size_t>(pin.layerMaterials()[j].subMaterialIndex), subLayer);
      subPinAuxs.push_back(subLayer.as<EntityRef>());
    }
    if (pin.cellMaterialIndex()) // 0 index means no material
    {
      // Append a material layer after the last layer
      AuxiliaryGeometry materialLayer = pinAux.resource()->addAuxiliaryGeometry(pinAux, 3);
      std::string materialName =
        pin.name() + smtk::session::rgg::Pin::subpartDescription +
          std::to_string(i) + Pin::materialDescription;
      materialLayer.setName(materialName);
      materialLayer.setStringProperty("rggType", Pin::typeDescription);
      assignColor(size_t(pin.cellMaterialIndex()), materialLayer);
      subPinAuxs.push_back(materialLayer.as<EntityRef>());
    }
  }
}

void OperationHelper::populateChildrenInDuctAux(
   AuxiliaryGeometry& ductAux, std::vector<EntityRef>& subAuxGeoms,const Duct& duct)
{
  ductAux.setStringProperty("rggType", smtk::session::rgg::Duct::typeDescription);

  auto assignColor = [](size_t index, smtk::model::AuxiliaryGeometry& aux) {
    smtk::model::FloatList rgba = smtk::session::rgg::CreateModel::getMaterialColor(index, aux.owningModel());
    aux.setColor(rgba);
  };

  // Create auxgeom placeholders for layers and parts
  const std::vector<Duct::Segment>& segments = duct.segments();
  for (std::size_t i = 0; i < segments.size(); i++)
  {
    const auto& seg = segments[i];
    for (std::size_t j = 0; j < static_cast<std::size_t>(seg.layers.size()); j++)
    {
      // Create an auxo_geom for every layer in current segment
      AuxiliaryGeometry subLayer = ductAux.resource()->addAuxiliaryGeometry(ductAux, 3);
      std::string subLName = duct.name() + smtk::session::rgg::Duct::segmentDescription + std::to_string(i) +
        smtk::session::rgg::Duct::layerDescription + std::to_string(j);
      subLayer.setName(subLName);
      subLayer.setStringProperty("rggType", smtk::session::rgg::Duct::typeDescription);
      assignColor(static_cast<size_t>(std::get<0>(seg.layers[j])), subLayer);
      subAuxGeoms.push_back(subLayer.as<EntityRef>());
    }
  }
}


} // namespace rgg
} //namespace session
} // namespace smtk
