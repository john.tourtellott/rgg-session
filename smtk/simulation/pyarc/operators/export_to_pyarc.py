#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

""" export_to_pyarc.py:

Export an RGG model as PyARC geometry.

"""
import sys

from . import export_to_pyarc_xml

import os
import rggsession
import smtk
import smtk.io
import smtk.model
from .convert_to_pyarc import *
from .pyarc_writer import *

class export_to_pyarc(smtk.operation.Operation):

    def __init__(self):
        smtk.operation.Operation.__init__(self)

    def name(self):
        return "export to pyarc"

    def operateInternal(self):

        # Access the PyARC SON file
        filename = self.parameters().find('filename').value(0)

        # Access the associated model
        model = smtk.model.Entity.CastTo(self.parameters().associations().objectValue(0))
        mcc3 = smtk.attribute.Attribute.CastTo(self.parameters().find('mcc3').objectValue(0))
        dif3d = smtk.attribute.Attribute.CastTo(self.parameters().find('dif3d').objectValue(0))

        writer = PyARCWriter()
        writer.logger = self.log()
        success = writer(filename, model, mcc3, dif3d)

        if success:
            return self.createResult(smtk.operation.Operation.Outcome.SUCCEEDED)
        else:
            return self.createResult(smtk.operation.Operation.Outcome.FAILED)

    def createSpecification(self):
        spec = smtk.attribute.Resource.create()
        reader = smtk.io.AttributeReader()
        reader.readContents(
            spec, export_to_pyarc_xml.description, self.log())
        return spec
