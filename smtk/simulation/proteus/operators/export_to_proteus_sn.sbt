<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="export-to-proteus_sn" Label="Export to Proteus-SN" BaseType="operation">
      <ItemDefinitions>
        <Component Name="model" Label="Model">
          <Accepts>
            <Resource Name="smtk::session::rgg::Resource" Filter="model"/>
          </Accepts>
        </Component>
        <Resource Name="mesh" Label="Mesh">
          <Accepts>
            <Resource Name="smtk::mesh::Resource"/>
          </Accepts>
        </Resource>
        <Resource Name="attributes" Label="Attributes">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <Directory Name="OutputDirectory" Label="Output Directory" NumberOfRequiredValues="1">
          <BriefDescription>The directory to write</BriefDescription>
        </Directory>
      </ItemDefinitions>
    </AttDef>
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(export-to-proteus_sn)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
